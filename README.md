# Design Project

A git project detailing my Pi-Hat

## Description
Our PiHat is a smoke and gas sensor that can be used in residential premises to notify users incase there's a gas leakage / fire. It's small enough to be used conveniently

## How to contribute to this repo
1. Clone the repo
```ssh
git clone git@gitlab.com:gmxpiu001/design-project.git
```
2. Create a _feature_ branch.
```ssh
git checkout -b feature-branch
```
3. Add your contribution to the _feature_ branch.
4. Push the _feature-branch_ to your gitlab profile.
5. Create a pull request into the _dev_ branch from your _feature-branch_ 
